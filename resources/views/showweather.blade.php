@extends('layout.header')

@section('content')
    
  <table class="table table-striped">
    <thead>
      <tr>
        <th>City Name </th>
        <th>Celsius   </th>
        <th>Time      </th>
      </tr>
    </thead>
    <tbody>
    	@foreach ($weather as $list)
	      <tr>
	        <td>{{ $list->city_name  }}</td>
	        <td>{{ $list->celsius    }}</td>
	        <td>{{ $list->created_at }}</td>
	      </tr>
		@endforeach
    </tbody>
  </table>

  <div class="form-control" id="wed_pegination">
    {{ $weather->links() }}
  </div>

@endsection