<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RakibDevs\Weather\Weather;
use App\Models\Weathercheck;

class WeathercheckController extends Controller
{

	public function Check_weather(Request $r)
	{
		$city_name = $r->input('city_name');
		$wt        = new Weather();
		$info      = $wt->getCurrentByCity($city_name);
		$new_query = array(
			'city_name' => $info->name,
			'celsius'   => $info->main->temp, 
		);

		Weathercheck::insert($new_query);
		echo json_encode($info);
	}

	public function Check_list()
	{
		$list = Weathercheck::orderBy('id', 'desc')->paginate(8);
		return  view('showweather',['weather' => $list]); 
	}
}
