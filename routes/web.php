<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WeathercheckController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('check_weather', [WeathercheckController::class, 'Check_weather']);
Route::get('check_list',     [WeathercheckController::class, 'Check_list']);